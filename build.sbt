name := "scala01"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "org.apache.poi" % "poi-ooxml" % "4.1.1"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.7"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % Test