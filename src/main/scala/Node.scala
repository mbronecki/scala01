case class Node(id: Int, name: String, level: Int, nodes: List[Node] = List.empty)
