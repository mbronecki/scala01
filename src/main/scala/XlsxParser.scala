
import org.apache.poi.ss.usermodel.CellType.{NUMERIC, STRING}
import org.apache.poi.ss.usermodel.Row

import scala.jdk.CollectionConverters._

object XlsxParser {

  def parseRow(row: Row): Option[Node] = {
    val cells = row.cellIterator().asScala.toList
    for {
      cellWithName <- cells.find(_.getCellType == STRING)
      cellWithId <- cells.find(_.getCellType == NUMERIC)
    } yield {
      val id = cellWithId.getNumericCellValue.toInt
      val name = cellWithName.getStringCellValue
      val level = cellWithName.getColumnIndex + 1

      Node(id, name, level)
    }
  }
}
