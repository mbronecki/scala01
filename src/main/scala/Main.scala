import NodeHelper.calculateNodes
import org.apache.poi.ss.usermodel.{Workbook, WorkbookFactory}
import org.json4s._
import org.json4s.jackson.Serialization.write

import scala.jdk.CollectionConverters._

object Main {
  implicit val formats = DefaultFormats

  def main(args: Array[String]): Unit = {
    val workbook = load("test1.xlsx")
    val sheet = workbook.getSheetAt(0)
    val rows = sheet.rowIterator().asScala.toList

    val nodes = rows.map(row => XlsxParser.parseRow(row)).collect {
      case Some(node) => node
    }

    val parents = nodes.filter(_.level == 1)
    println(write(calculateNodes(parents, nodes)))
  }

  def load(fileName: String): Workbook =
    WorkbookFactory.create(getClass.getClassLoader.getResourceAsStream(fileName))
}
