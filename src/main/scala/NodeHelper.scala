object NodeHelper {
  def calculateNodes(nodesFromFirstLevel: List[Node], allNodes: List[Node]): List[Node] = {
    nodesFromFirstLevel.map(parent => Node(parent.id, parent.name, parent.level,
      calculateNodes(findChildNodes(parent, allNodes), allNodes)))
  }

  private def findChildNodes(parent: Node, allNodes: List[Node]): List[Node] = {
    allNodes.find(node => node.level == 1 && node.id > parent.id) match {
      case Some(nextParent) => allNodes.filter(node => parent.level == node.level - 1 && node.id > parent.id && node.id < nextParent.id)
      case None => allNodes.filter(node => parent.level == node.level - 1 && node.id > parent.id)
    }
  }
}
