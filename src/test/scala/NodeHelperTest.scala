import org.scalatest.funsuite.AnyFunSuite

class NodeHelperTest extends AnyFunSuite {

  test("NodeHelperTest.calculateNodes should find all child nodes for their parents") {
    val nodes = List(
      Node(1, "A", 1),
      Node(2, "AA", 2),
      Node(3, "AA1", 3),
      Node(4, "AA2", 3),
      Node(5, "AB", 2),
      Node(6, "B", 1),
      Node(7, "C", 1),
      Node(8, "CA", 2),
      Node(9, "CA1", 3),
      Node(10, "CA2", 3),
      Node(11, "D", 1),
      Node(12, "DA", 2))

    val mappedNodes = NodeHelper.calculateNodes(nodes.filter(_.level == 1), nodes)

    assert(mappedNodes.size == 4)
    //A
    assert(mappedNodes.head.nodes.size == 2)
    //AA
    assert(mappedNodes.head.nodes.head.nodes.size == 2)
    //A1
    assert(mappedNodes.head.nodes.head.nodes.head.nodes.isEmpty)
    //A2
    assert(mappedNodes.head.nodes.head.nodes(1).nodes.isEmpty)
    //AB
    assert(mappedNodes.head.nodes(1).nodes.isEmpty)
    //B
    assert(mappedNodes(1).nodes.isEmpty)
    //C
    assert(mappedNodes(2).nodes.size == 1)
    //CA
    assert(mappedNodes(2).nodes.head.nodes.size == 2)
    //CA1
    assert(mappedNodes(2).nodes.head.nodes.head.nodes.isEmpty)
    //CA2
    assert(mappedNodes(2).nodes.head.nodes(1).nodes.isEmpty)
    //D
    assert(mappedNodes(3).nodes.size == 1)
    //DA
    assert(mappedNodes(3).nodes.head.nodes.isEmpty)
  }
}
