import org.apache.poi.ss.usermodel.WorkbookFactory
import org.scalatest.funsuite.AnyFunSuite

import scala.jdk.CollectionConverters._

class XlsxParserTest extends AnyFunSuite {

  test("XlsxParserTest.parseRow should return only nodes from xlsx file") {
    val sheet = WorkbookFactory.create(getClass.getClassLoader.getResourceAsStream("test1.xlsx")).getSheetAt(0)
    val rows = sheet.rowIterator().asScala.toList

    val nodes = rows.tail.map(row => XlsxParser.parseRow(row)).collect {
      case Some(node) => node
    }

    assert(nodes.size == 12)
    assert(nodes.head == Node(1, "A", 1))
    assert(nodes(1) == Node(2, "AA", 2))
    assert(nodes(2) == Node(3, "AA1", 3))
    assert(nodes(3) == Node(4, "AA2", 3))
    assert(nodes(4) == Node(5, "AB", 2))
    assert(nodes(5) == Node(6, "B", 1))
    assert(nodes(6) == Node(7, "C", 1))
    assert(nodes(7) == Node(8, "CA", 2))
    assert(nodes(8) == Node(9, "CA1", 3))
    assert(nodes(9) == Node(10, "CA2", 3))
    assert(nodes(10) == Node(11, "D", 1))
    assert(nodes(11) == Node(12, "DA", 2))
  }

}
